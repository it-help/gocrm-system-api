<?php


namespace GoCRM\API\System;


use GoCRM\API\Http\Client\HttpClient;
use GoCRM\API\System\Core\SystemSettingInterface;

class SystemSettings
{
    /**
     * @var HttpClient
     */
    private HttpClient $client;

    /**
     * @var array
     */
    private array $settings;
    
    public function __construct(HttpClient $client)
    {
        $this->client = $client;
        $this->settings = [];
    }

    /**
     * @param string $settingClass
     */
    public function registerSetting(string $settingClass): void
    {
        if (class_exists($settingClass) &&
            (new \ReflectionClass($settingClass))->implementsInterface(SystemSettingInterface::class)) {
            $this->settings[$settingClass::propertyName()] = new $settingClass($this->client);
        }
    }

    /**
     * @param string $propertyName
     * @return mixed|null
     */
    public function __get(string $propertyName)
    {
        if (empty($propertyObject = $this->settings[$propertyName]??null)) {
            return null;
        }

        $methodName = 'get' . ucfirst($propertyName);
        if (!method_exists($propertyObject, $methodName)) {
            if (method_exists($propertyObject, '__invoke')) {
                return $propertyObject();
            }
            return null;
        }

        return  $propertyObject->$methodName();
    }

    /**
     * @param string $propertyName
     * @param $value
     */
    public function __set(string $propertyName, $value)
    {
        if (empty($propertyObject = $this->settings[$propertyName]??null)) {
            // throw new SystemSettingNotFoundException();
            return;
        }

        $methodName = 'set' . ucfirst($propertyName);
        if (!method_exists($propertyObject, $methodName)) {
            if (method_exists($propertyObject, '__invoke')) {
                $propertyObject($value);
            }
            return;
        }

        $propertyObject->$methodName($value);
    }


    public function loadAccount(int $accountId): SystemAccount
    {
        return new SystemAccount($this->client, $accountId);
    }
}

<?php


namespace GoCRM\API\System\Core;


use GoCRM\API\Http\Client\HttpClient;
use GoCRM\API\Http\HttpRequest;

abstract class SystemSettingAbstract implements SystemSettingInterface
{
    /**
     * @var HttpRequest 
     */
    protected $request;

    /**
     * @var string
     */
    protected $path = '/';

    public function __construct(HttpClient $client)
    {
        $request = new HttpRequest($client);
        $request->setPath($this->path);
        $this->request = $request;
    }
}

<?php


namespace GoCRM\API\System\Core;


interface SystemSettingInterface
{
    public static function propertyName(): string;
}
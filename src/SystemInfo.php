<?php


namespace GoCRM\API\System;


use GoCRM\API\System\Core\SystemSettingAbstract;

class SystemInfo extends SystemSettingAbstract
{
    protected $path = '/info';

    /**
     * @return string
     */
    public static function propertyName(): string
    {
        return 'info';
    }


    /**
     * @return array
     */
    public function getInfo()
    {
        $response = $this->request->get('/');
        return $response->data();
    }
}

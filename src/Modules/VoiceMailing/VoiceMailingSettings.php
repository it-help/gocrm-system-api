<?php


namespace GoCRM\API\System\Modules\VoiceMailing;


class VoiceMailingSettings
{
    /**
     * @var float|null
     */
    private ?float $priceRecognize = null;

    /**
     * @var float|null
     */
    private ?float $priceTextGenerate = null;

    /**
     * @var float|null
     */
    private ?float $priceSms = null;

    /**
     * @var float|null
     */
    private ?float $pricePhoneConfirm = null;

    /**
     * @var float|null
     */
    private ?float $priceRecognizeCestina = null;

    /**
     * @var float|null
     */
    private ?float $priceTextGenerateCestina = null;

    /**
     * @var float|null
     */
    private ?float $priceTextGeneratePremium = null;

    /**
     * @var float|null
     */
    private ?float $priceMinuteCall = null;

    /**
     * @return float|null
     */
    public function getPriceRecognize(): ?float
    {
        return $this->priceRecognize;
    }

    /**
     * @param float|null $priceRecognize
     */
    public function setPriceRecognize(?float $priceRecognize): void
    {
        $this->priceRecognize = $priceRecognize;
    }

    /**
     * @return float|null
     */
    public function getPriceTextGenerate(): ?float
    {
        return $this->priceTextGenerate;
    }

    /**
     * @param float|null $priceTextGenerate
     */
    public function setPriceTextGenerate(?float $priceTextGenerate): void
    {
        $this->priceTextGenerate = $priceTextGenerate;
    }

    /**
     * @return float|null
     */
    public function getPriceSms(): ?float
    {
        return $this->priceSms;
    }

    /**
     * @param float|null $priceSms
     */
    public function setPriceSms(?float $priceSms): void
    {
        $this->priceSms = $priceSms;
    }

    /**
     * @return float|null
     */
    public function getPricePhoneConfirm(): ?float
    {
        return $this->pricePhoneConfirm;
    }

    /**
     * @param float|null $pricePhoneConfirm
     */
    public function setPricePhoneConfirm(?float $pricePhoneConfirm): void
    {
        $this->pricePhoneConfirm = $pricePhoneConfirm;
    }

    /**
     * @return float|null
     */
    public function getPriceRecognizeCestina(): ?float
    {
        return $this->priceRecognizeCestina;
    }

    /**
     * @param float|null $priceRecognizeCestina
     */
    public function setPriceRecognizeCestina(?float $priceRecognizeCestina): void
    {
        $this->priceRecognizeCestina = $priceRecognizeCestina;
    }

    /**
     * @return float|null
     */
    public function getPriceTextGenerateCestina(): ?float
    {
        return $this->priceTextGenerateCestina;
    }

    /**
     * @param float|null $priceTextGenerateCestina
     */
    public function setPriceTextGenerateCestina(?float $priceTextGenerateCestina): void
    {
        $this->priceTextGenerateCestina = $priceTextGenerateCestina;
    }

    /**
     * @return float|null
     */
    public function getPriceTextGeneratePremium(): ?float
    {
        return $this->priceTextGeneratePremium;
    }

    /**
     * @param float|null $priceTextGeneratePremium
     */
    public function setPriceTextGeneratePremium(?float $priceTextGeneratePremium): void
    {
        $this->priceTextGeneratePremium = $priceTextGeneratePremium;
    }

    /**
     * @return float|null
     */
    public function getPriceMinuteCall(): ?float
    {
        return $this->priceMinuteCall;
    }

    /**
     * @param float|null $priceMinuteCall
     */
    public function setPriceMinuteCall(?float $priceMinuteCall): void
    {
        $this->priceMinuteCall = $priceMinuteCall;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'price_recognize' => $this->priceRecognize,
            'price_text_generate' => $this->priceTextGenerate,
            'price_sms' => $this->priceSms,
            'price_phone_confirm' => $this->pricePhoneConfirm,
            'price_recognize_cestina' => $this->priceRecognizeCestina,
            'price_text_generate_cestina' => $this->priceTextGenerateCestina,
            'price_text_generate_premium' => $this->priceTextGeneratePremium,
            'price_minute_call' => $this->priceMinuteCall,
        ];
    }

    public function fill(array $attributes = []): self
    {
        $this->setPriceRecognize($attributes['price_recognize']);
        $this->setPriceTextGenerate($attributes['price_text_generate']);
        $this->setPriceSms($attributes['price_sms']);
        $this->setPricePhoneConfirm($attributes['price_phone_confirm']);
        $this->setPriceRecognizeCestina($attributes['price_recognize_cestina']);
        $this->setPriceTextGenerateCestina($attributes['price_text_generate_cestina']);
        $this->setPriceTextGeneratePremium($attributes['price_text_generate_premium']);
        $this->setPriceMinuteCall($attributes['price_minute_call']);

        return $this;
    }
}
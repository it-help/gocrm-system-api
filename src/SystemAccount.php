<?php


namespace GoCRM\API\System;


use GoCRM\API\Http\Client\HttpClient;
use GoCRM\API\Http\HttpRequest;

class SystemAccount
{
    /**
     * @var HttpRequest
     */
    private HttpRequest $request;

    /**
     * @var int
     */
    private int $accountId;

    /**
     * SystemAccount constructor.
     * @param HttpClient $client
     * @param int $accountId
     */
    public function __construct(HttpClient $client, int $accountId)
    {
        $request = new HttpRequest($client);
        $request->setPath('system/services/account');
        $this->request = $request;

        $this->accountId = $accountId;
    }

    /**
     * @param float $amount
     * @return bool
     * @throws \GoCRM\API\Http\Exceptions\GoCRMHttpResponseException
     */
    public function refill(float $amount): bool
    {
        $response = $this->request->post($this->accountId.'/fill',[], [
            'amount' => $amount
        ]);
        return $response->getStatus() === 'success';
    }
}
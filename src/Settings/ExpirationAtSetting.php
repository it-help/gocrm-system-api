<?php


namespace GoCRM\API\System\Settings;


use GoCRM\API\System\Core\SystemSettingAbstract;

class ExpirationAtSetting extends SystemSettingAbstract
{
    protected $path = 'system/settings';

    public static function propertyName(): string
    {
        return 'expirationAt';
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getExpirationAt(): ?\DateTimeInterface
    {
        $response = $this->request->get('/system_expiration_at');
        $systemExpirationAt = $response->data()['system_expiration_at']??null;
        
        return empty($systemExpirationAt) ? null :
            \DateTime::createFromFormat('Y-m-d H:i:s', $systemExpirationAt);
    }

    /**
     * @param \DateTimeInterface
     * @return bool
     */
    public function setExpirationAt(\DateTimeInterface $expirationAt): bool
    {
        $response = $this->request->put('/', [], [
            'system_expiration_at' => $expirationAt->format('Y-m-d H:i:s')
        ]);

        return $response->getStatus() === 'success';
    }
}

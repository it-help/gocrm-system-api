<?php


namespace GoCRM\API\System\Settings;


use GoCRM\API\System\Core\SystemSettingAbstract;

class VoiceMailingSetting extends SystemSettingAbstract
{
    protected $path = 'system/settings';

    public static function propertyName(): string
    {
        return 'voiceMailing';
    }

    /**
     * @return bool
     */
    public function getVoiceMailing(): bool
    {
        $response = $this->request->get('/voice_mailing');
        return !empty($response->data()['voice_mailing']);
    }

    /**
     * @param bool
     * @return bool
     */
    public function setVoiceMailing(bool $voiceMailing): bool
    {
        $response = $this->request->put('/', [], [
            'voice_mailing' => $voiceMailing
        ]);

        return $response->getStatus() === 'success';
    }
}